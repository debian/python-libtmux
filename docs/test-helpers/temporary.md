# Temporary Objects

Context managers for temporary tmux objects (sessions, windows).

```{eval-rst}
.. automodule:: libtmux.test.temporary
   :members:
   :undoc-members:
   :show-inheritance:
``` 