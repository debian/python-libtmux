# Environment

Environment variable mocking utilities for tests.

```{eval-rst}
.. automodule:: libtmux.test.environment
   :members:
   :undoc-members:
   :show-inheritance:
``` 